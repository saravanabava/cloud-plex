import { Component, OnInit } from '@angular/core';
import { MovieService } from '../inner-page/movie-list/movie.service';
declare const VdoPlayer:any;
@Component({
  selector: 'app-play-movie',
  templateUrl: './play-movie.component.html',
  styleUrls: ['./play-movie.component.css']
})
export class PlayMovieComponent implements OnInit {

  movie_details:any;
  otp="";
  playbackInfo="";
  constructor(private movie_service:MovieService) { }

  ngOnInit() {
    this.movie_details=JSON.parse(localStorage.getItem("movie"));
    this.getOtp();
  }

  getOtp()
  {

    this.movie_service.getOtp(this.movie_details.video.video_id).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        this.otp=jsonData.result.otp;
        this.playbackInfo=jsonData.result.playbackInfo;
        var video = new VdoPlayer({
          otp: this.otp,
          playbackInfo: btoa(JSON.stringify({
            videoId: this.movie_details.video.video_id
          })),
          theme: "9ae8bbe8dd964ddc9bdb932cca1cb59a",
          // the container can be any DOM element on website
          container: document.querySelector( "#embedBox" ),
        });
          video.addEventListener(`load`, () => {
            video.play(); // this will auto-start the video
            console.log('loaded');
          });
        console.log("Movies OTP =>"+JSON.stringify(jsonData));
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       console.log("Error=>"+JSON.stringify(error));
      }
    );
  }
}
