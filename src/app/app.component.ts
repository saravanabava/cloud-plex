import { Component } from '@angular/core';
import { Router, NavigationStart, Event, } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cloudplex';
  login=false;
  currentUrl="";
  constructor(private router:Router){

    this.router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
      
    
        this.currentUrl = routerEvent.url;
      
        console.log("Router Text=>"+this.currentUrl);
      }
     
      
    });

    console.log(this.router.url);

    if(this.router.url=="/")
    {
      this.login=true;
    }
    else{
      this.login=false;
    }
  }

  getClass(cur_url)
  {
    if(cur_url=="/" ||  cur_url=='/otp' || cur_url=='/reset-password')
    {
      return "col-12";
    }
    else{
      return "col-10";
    }
  }
}
