import { Component, OnInit } from '@angular/core';
import { UtilService } from 'src/app/util.service';
import { MovieService } from './movie.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { interval } from 'rxjs';
import { AuthService } from 'src/app/form-component/auth.service';
declare const VdoPlayer:any;
@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movieArray=[];
  country="";
  playbackInfo: any;
  otp: any;
  movie_name:string;
  show_video: boolean;
  constructor(private util_service:UtilService,
    private movie_service:MovieService,
    private auth_service:AuthService,
    private router:Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.country=localStorage.getItem('country');
    if(this.country!=null)
    {
      this.getMovies();
    }
    let auth_service=this.auth_service;
    let last_login=localStorage.getItem("last_login");
    let router=this.router;  

    //checking for new session 
    let interval=setInterval(function() {
      
      let user_id=localStorage.getItem('user_id');

      auth_service.checkRecentLogin(user_id).subscribe(
        (res) => {
         
          var jsonObject = JSON.stringify(res);
          var jsonData = JSON.parse(jsonObject);       
       //   console.log(JSON.stringify(jsonData));

          if(jsonData.result>last_login)
          {
            auth_service.logout().subscribe((res) => {
              if (!res.success) {
                clearInterval(interval);
                router.navigate(['']);
              }
            });
          }
        
        },
        (error) => {
         
        }
      );
    }, 1000);
  }




  getMovies()
  {
    this.movieArray=[];
    let payload={};
    payload["producer_id"]=this.util_service.producer_id;
     payload['user_id']=localStorage.getItem('user_id');
   
     console.log("current country=>"+this.country);
     if(localStorage.getItem('country')=="India")
     {
        payload['india']=true;
        payload['roi']=false;
     }
     else{
      payload['india']=false;
      payload['roi']=true;
     }

     this.movie_service.getMovies(JSON.stringify(payload)).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        this.movieArray=jsonData.result;
      //  console.log(JSON.stringify(this.movieArray));
        var d = new Date(); 
        var n = d.toUTCString(); 

        for(var i=0;i<this.movieArray.length;i++)
        {
          
          var end_time = new Date(this.movieArray[i].end_time);
         
          const diffTime = Math.abs(Date.parse(end_time.toDateString()) - Date.parse(n) );
          
               let totalSeconds     = Math.floor((end_time.getTime() - new Date(n).getTime())/1000);
            
               if(totalSeconds>0)
               {
                this.movieArray[i]["watch_movie"]="true";


               }
               else{
                this.movieArray[i]["watch_movie"]="false";
               }
          
        }
        
       
      
      },
      (error) => {
       
      }
    );

  
    
  }
 
  selectedMovie(movie_details,item)
  {
   // console.log("Selected Movie=>"+JSON.stringify(movie_details));
    localStorage.setItem("movie",JSON.stringify(movie_details));
    if(item=="movie_details")
    {
      this.router.navigate(['/movie-details'])
    }
    else if(item=="order"){
      this.router.navigate(['/order-summary'])
    }
    

  }

  getOtp(video_id,movie_name)
  {

    this.movie_name=movie_name;
    this.movie_service.getOtp(video_id).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
       
        let otp_result=JSON.parse(jsonData.result);
        this.otp=otp_result.otp;
       
        this.playbackInfo=jsonData.result.playbackInfo;
        var video = new VdoPlayer({
          otp: this.otp,
          playbackInfo: btoa(JSON.stringify({
            videoId: video_id
          })),
          theme: "9ae8bbe8dd964ddc9bdb932cca1cb59a",
          // the container can be any DOM element on website
          container: document.querySelector( "#embedBox" ),
        });
        video.addEventListener(`load`, () => {
          video.play(); // this will auto-start the video          
        });
      
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       
      }
    );
  }
  closeVideo()
  {
    this.show_video=false;
    this.stopVideo();
  }
  stopVideo(){
    var video = document.getElementById("embedBox");
    video.removeChild(video.childNodes[0]);
  }

}
