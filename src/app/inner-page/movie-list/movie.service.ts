import { Injectable, Inject } from '@angular/core';

import { UtilService } from 'src/app/util.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  token:any;
  constructor(@Inject(HttpClient) private http: HttpClient,
  private util_service:UtilService) { 

    this.token=localStorage.getItem('ingress');

  }

  getMovies(paylod)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.post(this.util_service.url + 'movie/producer',paylod,
    {headers,responseType:"json" });
  }

  getPurchasedMovies(user_id)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.get(this.util_service.url + 'movie/'+this.util_service.producer_id+"/user/"+user_id,
    {headers,responseType:"json" });
  }

  getOtp(movie_id)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.get(this.util_service.url + 'movie/'+movie_id+'/otp',
    {headers,responseType:"json" });
  }
}
