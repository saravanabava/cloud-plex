import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie-list/movie.service';
declare const VdoPlayer:any;
@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  country: string;
  show_video=false;

  movie_details:any;
  otp="";
  playbackInfo="";
  movie_name: any;
  constructor(private movie_service:MovieService) { }

  ngOnInit() {



    this.movie_details=JSON.parse(localStorage.getItem("movie"));
    var d = new Date(); 
    var n = d.toUTCString(); 

     
      var end_time = new Date(this.movie_details.end_time);
      
      const diffTime = Math.abs(Date.parse(end_time.toDateString()) - Date.parse(n) );
     
           let totalSeconds     = Math.floor((end_time.getTime() - new Date(n).getTime())/1000);
           
           if(totalSeconds>0)
           {
            this.movie_details["watch_movie"]="true";


           }
           else{
            this.movie_details["watch_movie"]="false";
           }
      
    
    this.country=localStorage.getItem('country');
  }  

  watchNow()
  {
    //this.show_video=true;
    this.getOtp();
  }
  closeVideo()
  {
    this.show_video=false;
    this.stopVideo();
  }

  closeTrailer()
  {
    this.show_video=false;
    this.stopTrailer();
  }

  stopVideo(){
    var video = document.getElementById("embedBox");
    video.removeChild(video.childNodes[0]);
  }
  stopTrailer(){
    var video = document.getElementById("#trailerBox");
    video.removeChild(video.childNodes[0]);
  }

  getOtp()
  {

    this.movie_service.getOtp(this.movie_details.video.video_id).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
       
        let otp_result=JSON.parse(jsonData.result);
        this.otp=otp_result.otp;
       
        this.playbackInfo=jsonData.result.playbackInfo;
        var video = new VdoPlayer({
          otp: this.otp,
          playbackInfo: btoa(JSON.stringify({
            videoId: this.movie_details.video.video_id
          })),
          theme: "9ae8bbe8dd964ddc9bdb932cca1cb59a",
          // the container can be any DOM element on website
          container: document.querySelector( "#embedBox" ),
        });
        video.addEventListener(`load`, () => {
          video.play(); // this will auto-start the video          
        });
      
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       
      }
    );
  }

  getTrailerOtp(video_id,movie_name)
  {

    this.movie_name=movie_name;
    this.movie_service.getOtp(video_id).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
       
        let otp_result=JSON.parse(jsonData.result);
        this.otp=otp_result.otp;
       
        this.playbackInfo=jsonData.result.playbackInfo;
        var video = new VdoPlayer({
          otp: this.otp,
          playbackInfo: btoa(JSON.stringify({
            videoId: video_id
          })),
          theme: "9ae8bbe8dd964ddc9bdb932cca1cb59a",
          // the container can be any DOM element on website
          container: document.querySelector( "#trailerBox" ),
        });
        video.addEventListener(`load`, () => {
          video.play(); // this will auto-start the video          
        });
      
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       
      }
    );
  }
}

