import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/form-component/auth.service';
declare const document:any;
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  country:any;
  user_details={"result":{"full_name":""}};
  constructor(private auth_service:AuthService) { }

  ngOnInit() {
  this.country=localStorage.getItem("country");
    window.addEventListener('load', function() {
      document.querySelector('input[type="file"]').addEventListener('change', function() {
          if (this.files && this.files[0]) {
              var img = document.querySelector('img');  // $('img')[0]
              img.src = URL.createObjectURL(this.files[0]); // set src to blob url
              //img.onload = imageIsLoaded;
          }
      });
    });

   
    this.getUserDetails();
  }

  getUserDetails()
  {
    let user_id=localStorage.getItem('user_id');
    let token=localStorage.getItem('ingress');
    this.auth_service.getUserDetails(user_id,token).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
           
        this.user_details=jsonData;
        
        //localStorage.setItem("user_email",jsonData.result.email);
     //   localStorage.setItem("user_name",jsonData.result.full_name);
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       console.log("Error=>"+JSON.stringify(error));
      }
    );
  }
  

}
