import { Component, OnInit } from '@angular/core';
import { MovieService } from '../movie-list/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.css']
})
export class PurchaseHistoryComponent implements OnInit {

  movieArray=[];
  constructor(private movie_service:MovieService,
    private router:Router) { }

  ngOnInit() {
    this.getMovies();
  }


  getMovies()
  {
      this.movieArray=[]

      let user_id=localStorage.getItem("user_id");
     this.movie_service.getPurchasedMovies(user_id).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        this.movieArray=jsonData.result;
        var d = new Date(); 
        var n = d.toUTCString(); 

        for(var i=0;i<this.movieArray.length;i++)
        {
          
          var end_time = new Date(this.movieArray[i].end_time);
               let totalSeconds     = Math.floor((end_time.getTime() - new Date(n).getTime())/1000);
            
               if(totalSeconds>0)
               {
                this.movieArray[i]["watch_movie"]="true";
               }
               else{
                this.movieArray[i]["watch_movie"]="false";
               }
          
        }
       


       
      //  data(this.unMappedMoviesArray); 
      
      },
      (error) => {
      
      }
    );

   
    
  }

 diff_minutes(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
  
 }


  watchNow(movie)
  {
       localStorage.setItem("movie",JSON.stringify(movie));
       this.router.navigate(['/movie-details'])
   
  }

}
