import { Injectable, Inject } from '@angular/core';

import { UtilService } from 'src/app/util.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  token: string;

  constructor(@Inject(HttpClient) private http: HttpClient,
  private util_service:UtilService) { 
    this.token=localStorage.getItem('ingress');
  }

  getOrder(paylod)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.post(this.util_service.url + 'payment/order',paylod,
    {headers,responseType:"json" });
  }

  getProducerDetails()
  {

    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.get(this.util_service.url + 'producer/'+this.util_service.producer_id,
    {headers,responseType:"json" });
  }


  submitPayment(payload)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',this.token)     
return this.http.post(this.util_service.url + 'payment',payload,
    {headers,responseType:"json" });
  }
}
