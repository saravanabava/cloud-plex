import { Component, OnInit, NgZone } from '@angular/core';
import { PaymentService } from './payment.service';
import { WindowRefService } from './window-ref.service';
import { Route, Router } from '@angular/router';


declare var Razorpay: any;

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css'],
  providers: [WindowRefService]
})
export class OrderSummaryComponent implements OnInit {
  country: string;

  tax = 0;
  razor_id: any;

  constructor(public payment_service: PaymentService,
    private winRef: WindowRefService,
    private router:Router,
    private zone: NgZone
  ) { }

  movie_details: any;
  ngOnInit() {

    this.movie_details = {};
    this.movie_details = JSON.parse(localStorage.getItem("movie"));
   
    this.country = localStorage.getItem('country');

    if (this.country == "India") {
      let total_price = this.movie_details.inr + this.tax;
      this.movie_details["total_price"] = total_price;
    }
    else if (this.country != "India") {
      let total_price = this.movie_details.usd + this.tax;
      this.movie_details["total_price"] = total_price;
    }
    this.getProducerDetails();

  }

  createOrder() {

    let payload = {};
    payload["user_id"] = localStorage.getItem("user_id");
    if (this.country == "India") {
      payload["amount"] = this.movie_details.inr * 100;
      payload["currency"] = "INR";
    }
    else if (this.country != "India") {
      payload["amount"] = this.movie_details.inr * 100;
      payload["currency"] = "USD";
    }
    payload["movie_id"] = this.movie_details.mov_id;
    payload["producer_id"] = this.movie_details.producer.producer_id;
    
    this.payment_service.getOrder(JSON.stringify(payload)).subscribe(
      (res) => {

        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);


      
        //  data(this.unMappedMoviesArray);
        this.razropayCall(jsonData.result.order_id, jsonData.result.amount, jsonData.result.currency, jsonData.result.receipt,this.payment_service);

      },
      (error) => {
        
      }
    );

  }


  getProducerDetails() {
    this.payment_service.getProducerDetails().subscribe(
      (res) => {

        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);


       

        this.razor_id = jsonData.result.razor_pay_id;
        //  data(this.unMappedMoviesArray);

      },
      (error) => {
        
      }
    );
  }


  razropayCall(order_id, amount, currency, transaction_id,payment_service) {
    let zone=this.zone;
    let router=this.router;
    let movie_id = this.movie_details.mov_id;
    let transaction_payload;
    var options = {
      "key": this.razor_id, // Enter the Key ID generated from the Dashboard
      "amount": amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      "currency": currency,
      "name": this.movie_details.mov_name,
      "description": this.movie_details.mov_description,
      "image": "",
      "order_id": order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
      "handler": function (response) {

      
        transaction_payload = {};
        // {"txn_id":"v","user_id":"v","txn_time":"v","amount":"v","status":"success/failure"
        //,"movie_id":"v","payment_id":"v","order_id":"v","signature":"v"}
        transaction_payload['txn_id'] = transaction_id;
        transaction_payload['user_id'] = localStorage.getItem("user_id");
        transaction_payload['amount'] = amount;
        transaction_payload['status'] = "success";
        transaction_payload['movie_id'] = movie_id;
        transaction_payload['payment_id'] = response.razorpay_payment_id;
        transaction_payload['order_id'] = response.razorpay_order_id;
        transaction_payload['signature'] = response.razorpay_signature;
        transaction_payload['currency'] = currency;
 
        let result=""
        payment_service.submitPayment(JSON.stringify(transaction_payload)).subscribe(
          (res) => {
    
            var jsonObject = JSON.stringify(res);
            var jsonData = JSON.parse(jsonObject);
    
            result="success";
           
            //  data(this.unMappedMoviesArray);
            
            zone.run(() => {
              router.navigate(['/movie-list']);
          });
            
          },
          (error) => {
            result="failure";
            
          }
        );

     
        

      },
      "prefill": {
        "name": localStorage.getItem("user_name"),
        "email": localStorage.getItem("user_email"),
        "contact": ""
      },
      "notes": {
        "address": "Razorpay Corporate Office"
      },
      "theme": {
        "color": "#3399cc"
      }
    };
    var rzp1 = new this.winRef.nativeWindow.Razorpay(options);
    rzp1.on('payment.failed', function (response) {
      
      transaction_payload = {};  
      transaction_payload['txn_id'] = transaction_id;
      transaction_payload['user_id'] = localStorage.getItem("user_id");
      transaction_payload['amount'] = amount;
      transaction_payload['status'] = "success";
      transaction_payload['movie_id'] = this.movie_details.mov_id;
      transaction_payload['payment_id'] = response.razorpay_payment_id;
      transaction_payload['order_id'] = response.razorpay_order_id;
      transaction_payload['signature'] = response.razorpay_signature;
    
      payment_service.submitPayment(JSON.stringify(transaction_payload)).subscribe(
        (res) => {
  
          var jsonObject = JSON.stringify(res);
          var jsonData = JSON.parse(jsonObject);
  
  
       
        
  
        },
        (error) => {
          console.log("Error=>" + JSON.stringify(error));
        }
      );
    });
    rzp1.open();

  }


  paymentSubmit(payload)
  {

  }

}
