import { NgModule, ChangeDetectorRef } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponentComponent } from './form-component/form-component.component';
//import { InnerPageComponent } from './inner-page/inner-page/inner-page.component';
import { OtpComponent } from './otp/otp.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { MovieDetailsComponent } from './inner-page/movie-details/movie-details.component';
import { OrderSummaryComponent } from './inner-page/order-summary/order-summary.component';
import { PurchaseHistoryComponent } from './inner-page/purchase-history/purchase-history.component';
import { MovieListComponent } from './inner-page/movie-list/movie-list.component';
import { PlayMovieComponent } from './play-movie/play-movie.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


const routes: Routes = [
  {
    path: '',
    component: FormComponentComponent,
    pathMatch: 'full'   
  },
  {
    path: 'movie-list',
    component: MovieListComponent
  },
  {
    path: 'otp',
    component: OtpComponent
  },
  {
    path: 'reset-password',
    component: NewPasswordComponent   
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent   
  },
  {
    path: 'movie-details',
    component: MovieDetailsComponent   
  },
  {
    path: 'order-summary',
    component: OrderSummaryComponent   
  },
  {
    path: 'purchase-history',
    component: PurchaseHistoryComponent   
  },
  {
    path: 'plai-movie',
    component: PlayMovieComponent   
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
