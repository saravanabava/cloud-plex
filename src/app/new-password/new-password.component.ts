import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../form-component/auth.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {

  passwordForm:FormGroup;
  constructor(private authService:AuthService,
    private _from_builder:FormBuilder,
    private _snackBar:MatSnackBar,
    private router:Router) {

      this.passwordForm = this._from_builder.group({
        password: ['', Validators.required]
      });
     }

  ngOnInit() {
  }

  onSubmitPwd(passwordForm)
  {
    let payload={};
    payload=passwordForm.value;
    payload['email']=localStorage.getItem("user_mail");
    this.authService
    .resetPassword(JSON.stringify(payload))
    .subscribe(
      (res) => {
        
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        if(jsonData.status=="UNAUTHORIZED")
        {
          this.showNotification(
            'bg-red',
            jsonData.message,
            'bottom',
            'right'
          );
        }
        else if(jsonData.status=="OK"){
          this.showNotification(
            'bg-green',
            'Password Changed Successfully',
            'bottom',
            'right'
          );
          this.passwordForm.reset();
          this.router.navigate(['']);
        }
   
      },
      (error) => {
        console.log("Error=>"+JSON.stringify(error));
   
      //  this.error = "Invalid Credentials";
       // this.submitted = false;
       // localStorage.setItem('STATE', 'false');
      }
    );
  }


  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName
    });
  }
}
