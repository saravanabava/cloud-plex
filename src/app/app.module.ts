import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponentComponent } from './form-component/form-component.component';
import { HeaderComponent } from './inner-page/header/header.component';
import { SideBarComponent } from './inner-page/side-bar/side-bar.component';
import { MovieListComponent } from './inner-page/movie-list/movie-list.component';
import { InnerPageComponent } from './inner-page/inner-page/inner-page.component';
import { MovieDetailsComponent } from './inner-page/movie-details/movie-details.component';
import { OrderSummaryComponent } from './inner-page/order-summary/order-summary.component';
import { PurchaseHistoryComponent } from './inner-page/purchase-history/purchase-history.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OtpComponent } from './otp/otp.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { WindowRefService } from './inner-page/order-summary/window-ref.service';
import { PlayMovieComponent } from './play-movie/play-movie.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CountdownModule, CountdownGlobalConfig } from 'ngx-countdown';

@NgModule({
  declarations: [
    AppComponent,
    FormComponentComponent,
    HeaderComponent,
    SideBarComponent,
    MovieListComponent,
    InnerPageComponent,
    MovieDetailsComponent,
    OrderSummaryComponent,
    PurchaseHistoryComponent,
    OtpComponent,
    NewPasswordComponent,
    PlayMovieComponent,
    ChangePasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSnackBarModule,
    CountdownModule,
    ReactiveFormsModule
    
    
  ],
  providers: [
    WindowRefService,{ provide: CountdownGlobalConfig }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
