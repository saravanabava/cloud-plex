import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.css']
})
export class FormComponentComponent implements OnInit {

  loginForm:FormGroup;
  registerForm:FormGroup;
  forgotForm:FormGroup;
  submitted = false;
  error:string;
  signin=true;
  signup=false;
  forgot=false; 

  constructor(private _from_builder:FormBuilder,
    private authService:AuthService,
    private router:Router,
    private _snackBar: MatSnackBar) {    
   }

  ngOnInit() {

    this.loginForm = this._from_builder.group({
      // username: ['sat251207@gmail.com', Validators.required],
      // password: ['123456', Validators.required]
      username: ['', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]
      ],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.registerForm = this._from_builder.group({      
      email: ['',[ 
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]
      ],
      full_name: ['', Validators.required],
      password: ['', [
        Validators.required, 
        Validators.minLength(6)]
      ]
    });

    this.forgotForm = this._from_builder.group({      
      email: ['',[ 
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]
      ]
    });

    if(localStorage.getItem('STATE')==="true")
    {
      this.router.navigate(['/movie-list']);
    }
    else if(localStorage.getItem('STATE')==="false")
    {
      this.router.navigate(['']);
    }

    
  }
  get f() {
    return this.loginForm.controls;
  }
  get r() {
    return this.registerForm.controls;
  }
  get p() {
    return this.forgotForm.controls;
  }


  onSelectSignup()
  {
    this.signin=false;
    this.signup=true;
    this.forgot=false;
  }
  onSelectForgot()
  {
    this.signin=false;
    this.signup=false;
    this.forgot=true;
  }
  onSelectSignin()
  {
    this.signin=true;
    this.signup=false;
    this.forgot=false;
  }
  onLoggedIn()
  {
    let router=this.router;
      this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))
      let login_credentials_payload={};
      login_credentials_payload['username']=this.f.username.value;
      login_credentials_payload['password']=this.f.password.value;
      this.authService
        .login(login_credentials_payload)
        .subscribe(
          (res) => {
           
            var jsonObject = JSON.stringify(res);
            var jsonData = JSON.parse(jsonObject);
            console.log(JSON.stringify(jsonData));

            if(jsonData.status=="UNAUTHORIZED")
            {
              this.showNotification(
                'bg-red',
                jsonData.message,
                'bottom',
                'right'
              );
            }
            else if(jsonData.status=="OK"){
              
              localStorage.setItem('ingress', jsonData.result.token);
              localStorage.setItem('user_id',jsonData.result.user_id)
              localStorage.setItem('STATE', 'true');
              localStorage.setItem('last_login', jsonData.result.last_login);
              this.getUserDetails(jsonData.result.user_id, jsonData.result.token);
            
               
                if (!navigator.geolocation){
                  console.log("Geolocation is not supported by your browser");
                  router.navigate(['/movie-list']);
                } else {
                  navigator.geolocation.getCurrentPosition(success, error);
                }
                  
                function success(position:any) {
                  let long:any  = position.coords.latitude;
                  let lat:any = position.coords.longitude;
                  localStorage.setItem("lat",lat);
                  localStorage.setItem("long",long);
                  ipLookup(long, lat)
                }
                function error() {
                  console.log("Unable to retrieve your location");
                  router.navigate(['/movie-list']);
                }
                function ipLookup(long,lat) {
                  fetch('https://extreme-ip-lookup.com/json/')
                  .then( res => res.json())
                  .then(response => {
                    fallbackProcess(response)
                  })
                }    
                function fallbackProcess(response:any) {
                 
                  console.log("User Country=>"+response.country);
                  localStorage.setItem("country",response.country);
                  router.navigate(['/movie-list']);
                }
           
             
          
              
            }
            
          },
          (error) => {
           
            this.showNotification(
              'bg-red',
              'Invalid Credentials',
              'bottom',
              'right'
            );
           this.submitted = false;
            localStorage.setItem('STATE', 'false');
          }
        );
    
  }

  onSignedUp(registerForm)
  {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }
    let payload=registerForm.value;
    let role_array=["USER"];
    payload["roles"]=role_array;
    
    this.authService
    .signedUp(JSON.stringify(payload))
    .subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        if(jsonData.status=="CONFLICT")
        {
          this.showNotification(
            'bg-red',
            'E-mail Already Registered',
            'bottom',
            'right'
          );
        }
        else if(jsonData.status=="OK"){
          this.showNotification(
            'bg-green',
            'Registered Successfully!, Verification link sent to your mail id.',
            'bottom',
            'right'
          );
          this.registerForm.reset();
          this.onSelectSignin();
        }
   
      },
      (error) => {
         
        this.error = "Invalid Credentials";
        this.submitted = false;
        localStorage.setItem('STATE', 'false');
      }
    );
  }

  onForgotPassword(forgotForm)
  {
    this.submitted = true;
    // stop here if form is invalid
    if (this.forgotForm.invalid) {
        return;
    }
    let payload=forgotForm.value;

    this.authService
    .getOtp(JSON.stringify(payload))
    .subscribe(
      (res) => {
        
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        if(jsonData.status=="NOT_FOUND")
        {
          this.showNotification(
            'bg-red',
            jsonData.message,
            'bottom',
            'right'
          );
        }
        else if(jsonData.status=="OK"){
          this.showNotification(
            'bg-green',
            'OTP sent to your E-mail id',
            'bottom',
            'right'
          );
          this.forgotForm.reset();
          localStorage.setItem("user_mail",payload.email)
          this.router.navigate(['/otp']);
        }
   
      },
      (error) => {
          
        this.error = "Invalid Credentials";
        this.submitted = false;
        localStorage.setItem('STATE', 'false');
      }
    );

  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName
    });
  }

  getUserDetails(user_id,token)
  {

    this.authService.getUserDetails(user_id,token).subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);          
       
        
        localStorage.setItem("user_email",jsonData.result.email);
        localStorage.setItem("user_name",jsonData.result.full_name);
      //  data(this.unMappedMoviesArray);
      
      },
      (error) => {
       console.log("Error=>"+JSON.stringify(error));
      }
    );
  }

}
