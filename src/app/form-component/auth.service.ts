import { Injectable, Inject } from '@angular/core';

import { of } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { UtilService } from 'src/app/util.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogin = false;
  constructor(@Inject(HttpClient) private http: HttpClient,
  private util_service:UtilService) {}

  login(payload) {
   
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'auth/token', payload,
        {headers,responseType:"json" });


  }

  logout() {
    this.isLogin = false;
    localStorage.setItem('STATE', 'false');
    localStorage.removeItem('ingress');
    localStorage.removeItem('country');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_mail');
    localStorage.removeItem("movie")
    return of({ success: this.isLogin });
  }

  isLoggedIn() {
    const loggedIn = localStorage.getItem('STATE');
    if (loggedIn === 'true') {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
    return this.isLogin;
  }

  getUserDetails(user_id,token)
  {
    const headers = new HttpHeaders()  
    .set('Content-Type', 'application/json')   
    .set('Authorization',token)     
return this.http.get(this.util_service.url + 'user/'+user_id,
    {headers,responseType:"json" });
  }

  signedUp(payload)
  {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'user', payload,
        {headers,responseType:"json" });
  }

  getOtp(payload)
  {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'user/forgot_password', payload,
        {headers,responseType:"json" });
  }

  submitOtp(payload)
  {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'user/verify_otp', payload,
        {headers,responseType:"json" });
  }
  resetPassword(payload)
  {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'user/rest_password', payload,
        {headers,responseType:"json" });
  }

  changePassword(payload)
  {
    
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.post(this.util_service.url + 'user/change_password', payload,
        {headers,responseType:"json" });
  }

  checkRecentLogin(user_id)
  {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')        
    return this.http.get(this.util_service.url + 'user/timestamp/'+user_id,
        {headers,responseType:"json" });
  }
}

