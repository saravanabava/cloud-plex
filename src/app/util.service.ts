import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  url:string;
  producer_id:string;

  constructor() { 
    this.url="https://api.cloudplex.in/v1.0/";
    this.producer_id="852aca76-90a9-41a5-8fb6-afb52ce0ec89";
  }
}
