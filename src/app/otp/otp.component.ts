import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../form-component/auth.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  otpForm:FormGroup;

  constructor(private authService:AuthService,
    private _from_builder:FormBuilder,
    private _snackBar:MatSnackBar,
    private router:Router) {

      this.otpForm = this._from_builder.group({
        otp: ['', Validators.required]
      });

   }

  ngOnInit() {
  }

  onSubmitOtp(otpForm)
  {
    let payload={};
    payload=otpForm.value;
    payload['email']=localStorage.getItem("user_mail");
    
    this.authService
    .submitOtp(JSON.stringify(payload))
    .subscribe(
      (res) => {
       
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        if(jsonData.status=="UNAUTHORIZED")
        {
          this.showNotification(
            'bg-red',
            jsonData.message,
            'bottom',
            'right'
          );
        }
        else if(jsonData.status=="OK"){
          this.showNotification(
            'bg-green',
            'OTP verified Successfully',
            'bottom',
            'right'
          );
          this.otpForm.reset();
          this.router.navigate(['/reset-password']);
        }
   
      },
      (error) => {
        console.log("Error=>"+JSON.stringify(error));
   
      //  this.error = "Invalid Credentials";
       // this.submitted = false;
       // localStorage.setItem('STATE', 'false');
      }
    );
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName
    });
  }

}
