import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../form-component/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  passwordForm:FormGroup;
  constructor(private authService:AuthService,
    private _from_builder:FormBuilder,
    private _snackBar:MatSnackBar,
    private router:Router) {

      this.passwordForm = this._from_builder.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        old_password:['',[Validators.required, Validators.minLength(6)]]
      });
     }

  ngOnInit() {
  }
  onSubmitPwd(passwordForm)
  {
    let payload={};
    payload=passwordForm.value;
    payload['email']=localStorage.getItem("user_mail");
    console.log("Reset Password Result=>"+JSON.stringify(payload));
    this.authService
    .changePassword(JSON.stringify(payload))
    .subscribe(
      (res) => {
        console.log("Reset Password Result=>"+JSON.stringify(res));
        var jsonObject = JSON.stringify(res);
        var jsonData = JSON.parse(jsonObject);
        if(jsonData.status=="UNAUTHORIZED")
        {
          this.showNotification(
            'bg-red',
            jsonData.message,
            'bottom',
            'right'
          );
        }
        else if(jsonData.status=="CONFLICT"){
          this.showNotification(
            'bg-green',
            jsonData.message,
            'bottom',
            'right'
          );
        
        }
        else if(jsonData.status=="OK"){
          this.showNotification(
            'bg-green',
            'Password Changed Successfully',
            'bottom',
            'right'
          );
          this.passwordForm.reset();
          this.router.navigate(['/movie-list']);
        }
   
      },
      (error) => {
        console.log("Error=>"+JSON.stringify(error));
     this.showNotification(
            'bg-red',
            error.error.message,
            'bottom',
            'right'
          );
      //  this.error = "Invalid Credentials";
       // this.submitted = false;
       // localStorage.setItem('STATE', 'false');
      }
    );
  }


  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName
    });
  }
}
